attrdict==2.0.1
attrs==21.2.0
beautifulsoup4==4.9.3
coverage==5.5
docopt==0.6.2
et-xmlfile==1.1.0
gunicorn==20.1.0
hupper==1.10.3
iniconfig==1.1.1
jdcal==1.4.1
Jinja2==3.0.1
Mako==1.1.4
MarkupSafe==2.0.1
openpyxl==2.4.11
packaging==20.9
PasteDeploy==2.1.1
plaster==1.0
plaster-pastedeploy==0.7
pluggy==0.13.1
psycopg2-binary==2.8.6
py==1.10.0
Pygments==2.9.0
pyparsing==2.4.7
pyramid==2.0
pyramid-debugtoolbar==4.9
pyramid-jinja2==2.8
pyramid-mako==1.1.0
pytest==6.2.4
pytest-cov==2.12.1
python-dateutil==2.8.1
records==0.5.3
repoze.lru==0.7
six==1.16.0
soupsieve==2.2.1
SQLAlchemy==1.3.24
tablib==3.0.0
toml==0.10.2
translationstring==1.4
venusian==3.0.0
waitress==2.0.0
WebOb==1.8.7
WebTest==2.0.35
zope.deprecation==4.4.0
zope.interface==5.4.0
