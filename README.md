# IOCBIO WebDB

This is a web framework that can be used for development of
site-specific applications allowing you to fill enter the data into
the database and query it.

README is partitioned to simplify the use of this repository with the
derived web applications. See [WebDB README here](README.webdb.md).

