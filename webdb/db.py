# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2019-2020
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  This file is part of project: IOCBIO WebDB
#
# database i/o

from pyramid.paster import get_appsettings
from pyramid.threadlocal import get_current_registry
from records import Database

from .error import SqlError

def db_url(settings, username, password):
    return settings['database_template'].format(username, password)

def _query_u(_webdb_database, _webdb_database_user, statement, **options):
    """Query that first switches to the user role and
    then performs it"""
    with _webdb_database.get_connection() as conn:
        tx = conn.transaction()
        conn.query('SET LOCAL ROLE :user', user=_webdb_database_user)
        r = conn.query(statement, **options)
        tx.commit()
        return r

def query(request, statement, **options):
    _webdb_user = request.authenticated_userid
    _webdb_database = request.database
    try:
        return _query_u(_webdb_database, _webdb_user, statement, **options)
    except Exception as err:
        raise SqlError(str(err))

def check_user_login(settings, username, password):
    """Check if user can login with the provided password"""
    try:
        dbu = db_url(settings, username, password)
        db = Database(dbu)
        db.query('SELECT 1').all()
        db.close()
    except Exception as err:
        print('*********')
        print('Authentification failed')
        print(err)
        print('*********')
        return False
    return True

def check_web_use(database, user):
    try:
        _query_u(database, user, "SELECT 1").all()
    except Exception as err:
        print('*********')
        print('Cannot switch to user role')
        print(err)
        print('*********')
        return False
    return True
        
