jQuery(document).ready(function(){
    jQuery('.datepicker').pickadate({
        formatSubmit: 'yyyy-mm-dd',
        selectYears: true,
        selectMonths: true
    });
    // jQuery('.timepicker').pickatime({
    //     format: 'HH:i',
    //     formatSubmit: 'HH:i',
    //     interval: 1,
    //     editable: true
    // });
    jQuery('.timepicker').timepicker({
        timeFormat: 'HH:mm',
        minHour: 0,
        maxHour: 23,
        minMinutes: 0,
        maxMinutes: 59
    });
})
