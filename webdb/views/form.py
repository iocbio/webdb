# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2019-2020
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  This file is part of project: IOCBIO WebDB
#
# -*- coding: utf-8 -*-

from .table import TableView
from ..views.text import *
from ..misc import *
from ..db import query
from ..error import GenericError

class FormView(TableView):
    """Expands TableView for forms allowing to use dynamic fields"""

    def __init__(self, request):
        TableView.__init__(self, request)

    def fill_form(self, fields, title, edit_url=None, save_button_text = 'Submit'):
        """Fill form based on provided fields and request parameters"""
        
        # prepare selectors
        selections = {}
        for f in fields:
            if f.selector and not f.large_selection:
                s = f.selector
                if isinstance(s, str): selections[f.id] = [dict(value=q.value, text=q.text) for q in query(self.request, s)]
                else: selections[f.id] = copy.copy(s)
                selections[f.id].insert(0, dict(value='', text=''))

        # fill form fields
        d = []
        for f in fields:
            i = f.id
            t = Text()
            tags = f.tags

            # check if it has application-specific default
            if f.add_default is not None:
                if callable(f.add_default): t.text = f.add_default(self.request)
                else: t.text = f.add_default

            # check if value is specified in request
            if i in self.request.params: t.text = self.request.params[i]

            dateTime = True if tags == tagDateTime else False
            dateOnly = True if tags == tagDate else False
            trueFalse = True if tags == tagTrueFalse or tags == tagBool else False
            if (tags == tagTrueFalse and t.text=='T') or (tags == tagBool and t.text):
                checked = True
            else: checked = False
            readonly = True if f.readonly else False
            lst = self.field_selector_text(field=i, value=t.text) if f.large_selection and t.text else ''
            item = dict(id = i,
                        text = t,
                        readonly = readonly,
                        selection = selections.get(i, False),
                        largeSelection = f.large_selection,
                        largeSelectionText = lst,
                        field = f,
                        textArea = True if tags == tagText else False,
                        dateTime = dateTime, dateOnly = dateOnly,
                        trueFalse = trueFalse, checked = checked )

            if dateTime:
                item['date'] = t.text.date().isoformat() if t.text else ''
                item['time'] = t.text.time().isoformat(timespec='minutes') if t.text else ''
            if dateOnly:
                item['date'] = t.text.isoformat() if t.text else ''

            d.append(item)

        if not d: raise GenericError(title='Empty form',
                                     message='There is nothing to include in the form')
        
        return dict( title = title,
                     edit_url = edit_url if edit_url else self.request.current_route_url(),
                     data = d,
                     save_button_text = save_button_text,
                     back_to_list = self.back_to_list )

    
    def value2text(self, sql, value=None, key=None):
        """Use selector-type SQL statement to get human-readable selection value"""

        value = self.request.params.get(key, '') if key else value
        if not value:
            self.request.session.flash('Error: cannot query SQL without index value')
            raise GenericError(title = 'No value specified',
                               message = 'Value lookup failed as there was no value specified',
                               code = sql )

        return query(self.request,
                     "SELECT * FROM (%s) s WHERE value=:value" % sql,
                     value=value).first().text

