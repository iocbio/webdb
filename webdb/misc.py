# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2019-2020
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  This file is part of project: IOCBIO WebDB
#
# small helper routines and constants
# note that it is usually imported with *

def str2int(txt, default=0):
    try:
        return int(txt)
    except:
        pass
    return default

# tags used for html input
tagFloat = dict(type='number', step='any')
tagInt = dict(type='number', step='1')
tagText = dict(_textArea_=True)
tagDateTime = dict(_dateTime_=True)
tagDate = dict(_date_=True)
tagBool = dict(_boolean_=True)
tagTrueFalse = dict(_truefalse_=True)

isTrue = 'T'

# selectors
selTrueFalse = [ dict(value='F', text='F'), dict(value='T', text='T') ]
selMaleFemale = [ dict(value='Female', text='Female'), dict(value='Male', text='Male') ]
selMF  = [ dict(value='F', text='Female'), dict(value='M', text='Male') ]
