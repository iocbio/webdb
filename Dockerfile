FROM ubuntu:18.04

RUN apt-get update -y && \
    apt-get install -y python3-pip python3-dev && \
    pip3 install --upgrade pip setuptools

WORKDIR /app
COPY requirements.txt /app/requirements.txt
COPY setup.py README.md /app/
COPY webdb /app/webdb
COPY animaldb /app/animaldb

RUN pip3 install -r requirements.txt
RUN pip3 install -e .

EXPOSE 6543
